<?php

/**
 * @file
 * Definition of GSBMediaCenterDocument.
 */

/**
 * Media browser plugin for Media Documents.
 */
class PPMediaLibraryDocument extends MediaBrowserPlugin {
  /**
   * Implements MediaBrowserPlugin::view().
   */
  public function view() {
    module_load_include('inc', 'file_entity', 'file_entity.pages');

    $build = array();
    $params = $this->params;
    $params['file_extensions'] = 'txt doc docx xls xlsx pdf ppt pptx pps odt ods odp zip';

    $build['form']['document_upload'] = drupal_get_form('pp_media_library_add_files_and_docs_upload', $params);

    // We need the current step put on the form portion of the array.
    $build['form']['#step'] = $build['form']['document_upload']['#step'];
    if ($build['form']['document_upload']['#step'] == 1) {
      $build['form']['document_upload']['upload']['#weight'] = -3;
      $build['form']['document_upload']['private_upload']['#weight'] = -2;
      $build['form']['document_upload']['actions']['#weight'] = -1;

      $build['form']['document_upload']['upload']['upload_button']['#access'] = FALSE;

      $info['view_name'] = 'pp_media_browser';
      $info['view_display_id'] = 'files_and_docs';

      $view = new PPMediaLibraryView($info, $params);

      $build['form']['document_browser']['view'] = $view->view();
      $build['form']['document_browser']['fake_buttons']['#markup'] = $view->generateButtons();
    }

    return $build;

  }
}
