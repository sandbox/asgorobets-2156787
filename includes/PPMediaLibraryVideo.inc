<?php

/**
 * @file
 * Definition of GSBMediaCenterImage.
 */

/**
 * Media browser plugin for Media Video.
 */
class PPMediaLibraryVideo extends MediaBrowserPlugin {
  /**
   * Implements MediaBrowserPlugin::view().
   */
  public function view() {
    module_load_include('inc', 'file_entity', 'file_entity.pages');
    module_load_include('inc', 'media_internet', 'media_internet.pages');

    $build = array();
    $params = $this->params;

    $build['form']['video_upload'] = drupal_get_form('pp_media_library_add_video_upload', $params['types'], $params['multiselect']);

    $build['form']['video_upload']['embed_code']['#title'] = t('Enter a url to the video.');
    $providers = array();
    foreach ($build['form']['video_upload']['providers']['#items'] as $provider) {
      $providers[] = $provider['data'];
    }

    $provider_list = implode(', ', $providers);
    $build['form']['video_upload']['embed_code']['#description'] = t('Enter the URL to the video. You can use URL\'s from one of these places: @providers', array('@providers' => $provider_list));

    $build['form']['video_upload']['providers']['#access'] = FALSE;

    $build['form']['video_upload']['embed_code']['#weight'] = -2;
    $build['form']['video_upload']['actions']['#weight'] = -1;

    $build['form']['video_upload']['#submit'][] = 'pp_media_library_video_submit';

    $info['view_name'] = 'pp_media_browser';
    $info['view_display_id'] = 'videos';

    $view = new PPMediaLibraryView($info, $params);

    $build['form']['video_browser']['view'] = $view->view();
    $build['form']['video_browser']['fake_buttons']['#markup'] = $view->generateButtons();

    return $build;
  }
}
