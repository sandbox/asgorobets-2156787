<?php

/**
 * @file
 * Media module browser integration with focus for UX.
 */

/**
 * Implements hook_media_browser_plugin_info().
 */
function pp_media_library_media_browser_plugin_info() {
  $info = array();

  $info['pp_media_library_image'] = array(
    'title' => t('Images'),
    'class' => 'PPMediaLibraryImage',
  );

  $info['pp_media_library_document'] = array(
    'title' => t('Files'),
    'class' => 'PPMediaLibraryDocument',
  );

  $info['pp_media_library_video'] = array(
    'title' => t('Videos'),
    'class' => 'PPMediaLibraryVideo',
  );

  return $info;
}

/**
 * Implements hook_media_browser_plugins_alter().
 */
function pp_media_library_media_browser_plugins_alter(&$plugin_output) {
  $order = array(
    'pp_media_library_image',
    'pp_media_library_document',
    'pp_media_library_video',
  );

  foreach ($order as $index => $plugin) {
    if (isset($plugin_output[$plugin])) {
      $plugin_output[$plugin]['#weight'] = $index;
    }
  }
}
